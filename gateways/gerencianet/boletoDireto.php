<?php

require __DIR__.'/vendor/autoload.php';
require __DIR__.'/../../application/helpers/funcoes_helper.php';

use Gerencianet\Exception\GerencianetException;
use Gerencianet\Gerencianet;

$file = json_encode($_POST['config']);
$post = $_POST['post'];

$options = json_decode($file, true);
$valor_fatura = intval(number_format(floatval($post['Valor']), 2, '', ''));

$items = [
    [
        'name' => $post['Descricao'],
        'amount' => 1,
        'value' => $valor_fatura
    ]
];

$body = [
    'items' => $items
];

try {
    $api = new Gerencianet($options);
    $charge = $api->createCharge([], $body);
    
    $params = ['id' => $charge['data']['charge_id']];

    $juridical_data = [
        'corporate_name' => $post['clienteNome'],
        'cnpj' => limpaNumeros($post['clienteCNPJ'])
    ];

    if($post['clientePessoa'] == 'fisica'){
        $customer = [
            'name' => $post['clienteNome'],
            'cpf' => limpaNumeros($post['clienteCPF']),
            'phone_number' => $post['clienteDDD'].$post['clienteTel']
        ];
    }else{
        $customer = [
            'name' => $post['clienteNome'],
            'cpf' => limpaNumeros($post['clienteCPF']),
            'phone_number' => $post['clienteDDD'].$post['clienteTel'],
            'juridical_person' => $juridical_data
        ];
    }

    $body = [
        'payment' => [
            'banking_billet' => [
                'expire_at' => dateFormat($post['Vencimento'], 'Y-m-d'),
                'customer' => $customer
            ]
        ]
    ];

    try {
        $api = new Gerencianet($options);
        $response = $api->payCharge($params, $body);

        echo json_encode($response);

    } catch (GerencianetException $e) {

        echo json_encode($e->code);
        echo json_encode($e->error);
        echo json_encode($e->errorDescription);

    } catch (Exception $e) {
        echo json_encode($e->getMessage());
    }
} catch (GerencianetException $e) {

    echo json_encode($e->code);
    echo json_encode($e->error);
    echo json_encode($e->errorDescription);

} catch (Exception $e){
    echo json_encode($e->getMessage());
}
