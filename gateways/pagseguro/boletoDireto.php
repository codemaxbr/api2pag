<?php
require __DIR__.'/source/PagSeguroLibrary/PagSeguroLibrary.php';
require __DIR__.'/../../application/helpers/funcoes_helper.php';

$config = $_POST['config'];
$post = $_POST['post'];

if($config['sandbox'] == true){
	PagSeguroConfig::setEnvironment('sandbox');
}else{
	PagSeguroConfig::setEnvironment('production');
}

$directPaymentRequest = new PagSeguroDirectPaymentRequest();
$directPaymentRequest->setPaymentMode('DEFAULT');
$directPaymentRequest->setPaymentMethod('BOLETO');
$directPaymentRequest->setReceiverEmail($config['email']);
$directPaymentRequest->setCurrency("BRL");

// Add an item for this payment request
$directPaymentRequest->addItem(
    $post['faturaID'],
    $post['Descricao'],
    1,
    floatval($post['Valor'])
);

$directPaymentRequest->setReference("FAT".$post['faturaID']);

if($post['clienteCPF'] != ""){
    $label_cpf_cnpj = 'CPF';
    $cpf_cnpj = $post['clienteCPF'];
}else{
    $label_cpf_cnpj = 'CNPJ';
    $cpf_cnpj = $post['clienteCNPJ'];
}

$directPaymentRequest->setSender(
    $post['clienteNome'],
    $post['clienteEmail'],
    $post['clienteDDD'],
    $post['clienteTel'],
    $label_cpf_cnpj,
    $cpf_cnpj,
    true
);

$directPaymentRequest->setSenderHash($post['senderHash']);

// Set shipping information for this payment request
$sedexCode = PagSeguroShippingType::getCodeByType('SEDEX');
$directPaymentRequest->setShippingType($sedexCode);
$directPaymentRequest->setShippingAddress(
    $post['clienteCEP'],
    $post['clienteLogradouro'],
    $post['clienteNumero'],
    $post['clienteComplemento'],
    $post['clienteBairro'],
    $post['clienteCidade'],
    $post['clienteUF'],
    'BRA'
);

try {
    $credentials = new PagSeguroAccountCredentials($config['email'], $config['token']);

    // Register this payment request in PagSeguro to obtain the payment URL to redirect your customer.
    $return = $directPaymentRequest->register($credentials);

    switch ($return->getStatus()->getValue()) {
    	case 1: $status_pagseguro = 'Em aberto'; 	break;
    	case 2: $status_pagseguro = 'Em análise'; 	break;
    	case 3: $status_pagseguro = 'Pago'; 		break;
    	case 4: $status_pagseguro = 'Disponível'; 	break;
    	case 5: $status_pagseguro = 'Em disputa'; 	break;
    	case 6: $status_pagseguro = 'Devolvido'; 	break;
    	case 7: $status_pagseguro = 'Cancelado'; 	break;
    }

    $retorno = array(
    	'status' => 'sucesso',
    	'retorno' => array(
    			'hash' => $return->getCode(),
    			'fatura' => $post['faturaID'],
    			'forma_pagamento' => 'boleto',
    			'gateway' => 'pagseguro',
    			'link_pagamento' => $return->getPaymentLink(),
    			'valor' => $return->getGrossAmount(),
    			'cliente' => $post['clienteID'],
    			'status' => $status_pagseguro,
    			'conta_id' => $post['contaId']
    		)
    	);

    $api = xmlPOST(siteURL().'/api/novaTransacao', $retorno['retorno']);
    if($api->status == "sucesso"){
    	echo json_encode($retorno);
    }

} catch (PagSeguroServiceException $e) {
    //die($e->getMessage());
    $retorno = array(
    	'status' => 'erro',
    	'mensagem' => $e->getMessage()
    );

    echo json_encode($retorno);
}
