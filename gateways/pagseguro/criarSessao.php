<?php
require __DIR__.'/source/PagSeguroLibrary/PagSeguroLibrary.php';
require __DIR__.'/../../application/helpers/funcoes_helper.php';

$config = (object) $_POST['dados'];

if($config->sandbox == 1){
	PagSeguroConfig::setEnvironment('sandbox');
}else{
	PagSeguroConfig::setEnvironment('production');
}

try {
    $credentials = new PagSeguroAccountCredentials($config->email, $config->token);
    $session = PagSeguroSessionService::getSession($credentials);

    echo $session;

} catch (PagSeguroServiceException $e) {
    die($e->getMessage());
}
