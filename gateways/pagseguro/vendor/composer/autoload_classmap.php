<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'LogPagSeguro' => $baseDir . '/source/PagSeguroLibrary/log/LogPagSeguro.class.php',
    'PagSeguroAcceptPaymentMethod' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroAcceptPaymentMethod.class.php',
    'PagSeguroAcceptedPaymentMethods' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroAcceptedPaymentMethods.class.php',
    'PagSeguroAcceptedPayments' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroAcceptedPayments.class.php',
    'PagSeguroAccountCredentials' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroAccountCredentials.class.php',
    'PagSeguroAddress' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroAddress.class.php',
    'PagSeguroApplicationCredentials' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroApplicationCredentials.class.php',
    'PagSeguroAuthorization' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroAuthorization.class.php',
    'PagSeguroAuthorizationAccount' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroAuthorizationAccount.class.php',
    'PagSeguroAuthorizationParser' => $baseDir . '/source/PagSeguroLibrary/parser/PagSeguroAuthorizationParser.class.php',
    'PagSeguroAuthorizationPermission' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroAuthorizationPermission.class.php',
    'PagSeguroAuthorizationPermissions' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroAuthorizationPermissions.class.php',
    'PagSeguroAuthorizationRequest' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroAuthorizationRequest.class.php',
    'PagSeguroAuthorizationSearchResult' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroAuthorizationSearchResult.class.php',
    'PagSeguroAuthorizationSearchService' => $baseDir . '/source/PagSeguroLibrary/service/PagSeguroAuthorizationSearchService.class.php',
    'PagSeguroAuthorizationService' => $baseDir . '/source/PagSeguroLibrary/service/PagSeguroAuthorizationService.class.php',
    'PagSeguroAutoloader' => $baseDir . '/source/PagSeguroLibrary/loader/PagSeguroAutoLoader.class.php',
    'PagSeguroBilling' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroBilling.class.php',
    'PagSeguroCancelParser' => $baseDir . '/source/PagSeguroLibrary/parser/PagSeguroCancelParser.class.php',
    'PagSeguroCancelService' => $baseDir . '/source/PagSeguroLibrary/service/PagSeguroCancelService.class.php',
    'PagSeguroConfig' => $baseDir . '/source/PagSeguroLibrary/config/PagSeguroConfig.class.php',
    'PagSeguroConfigWrapper' => $baseDir . '/source/PagSeguroLibrary/config/PagSeguroConfigWrapper.php',
    'PagSeguroConnectionData' => $baseDir . '/source/PagSeguroLibrary/service/PagSeguroConnectionData.class.php',
    'PagSeguroCredentials' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroCredentials.class.php',
    'PagSeguroCreditCard' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroCreditCard.class.php',
    'PagSeguroCreditCardCheckout' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroCreditCardCheckout.class.php',
    'PagSeguroCreditCardHolder' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroCreditCardHolder.class.php',
    'PagSeguroCurrencies' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroCurrencies.class.php',
    'PagSeguroDirectPaymentInstallment' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroDirectPaymentInstallment.class.php',
    'PagSeguroDirectPaymentMethods' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroDirectPaymentMethods.class.php',
    'PagSeguroDirectPaymentParser' => $baseDir . '/source/PagSeguroLibrary/parser/PagSeguroDirectPaymentParser.class.php',
    'PagSeguroDirectPaymentRequest' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroDirectPaymentRequest.class.php',
    'PagSeguroDirectPaymentService' => $baseDir . '/source/PagSeguroLibrary/service/PagSeguroDirectPaymentService.class.php',
    'PagSeguroDocument' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroDocument.class.php',
    'PagSeguroDocuments' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroDocuments.class.php',
    'PagSeguroError' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroError.class.php',
    'PagSeguroExcludePaymentMethod' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroExcludePaymentMethod.class.php',
    'PagSeguroHelper' => $baseDir . '/source/PagSeguroLibrary/helper/PagSeguroHelper.class.php',
    'PagSeguroHttpConnection' => $baseDir . '/source/PagSeguroLibrary/utils/PagSeguroHttpConnection.class.php',
    'PagSeguroHttpStatus' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroHttpStatus.class.php',
    'PagSeguroInstallment' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroInstallment.class.php',
    'PagSeguroInstallmentParser' => $baseDir . '/source/PagSeguroLibrary/parser/PagSeguroInstallmentParser.class.php',
    'PagSeguroInstallmentService' => $baseDir . '/source/PagSeguroLibrary/service/PagSeguroInstallmentService.class.php',
    'PagSeguroInstallments' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroInstallments.class.php',
    'PagSeguroItem' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroItem.class.php',
    'PagSeguroLibrary' => $baseDir . '/source/PagSeguroLibrary/PagSeguroLibrary.class.php',
    'PagSeguroMetaData' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroMetaData.class.php',
    'PagSeguroMetaDataItem' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroMetaDataItem.class.php',
    'PagSeguroMetaDataItemKeys' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroMetaDataItemKeys.class.php',
    'PagSeguroNotificationService' => $baseDir . '/source/PagSeguroLibrary/service/PagSeguroNotificationService.class.php',
    'PagSeguroNotificationType' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroNotificationType.class.php',
    'PagSeguroOnlineDebitCheckout' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroOnlineDebitCheckout.class.php',
    'PagSeguroParameter' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroParameter.class.php',
    'PagSeguroParameterItem' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroParameterItem.class.php',
    'PagSeguroParserData' => $baseDir . '/source/PagSeguroLibrary/parser/PagSeguroParserData.class.php',
    'PagSeguroPaymentMethod' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroPaymentMethod.class.php',
    'PagSeguroPaymentMethodCode' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroPaymentMethodCode.class.php',
    'PagSeguroPaymentMethodConfig' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroPaymentMethodConfig.class.php',
    'PagSeguroPaymentMethodConfigItem' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroPaymentMethodConfigItem.class.php',
    'PagSeguroPaymentMethodConfigKeys' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroPaymentMethodConfigKeys.class.php',
    'PagSeguroPaymentMethodGroups' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroPaymentMethodGroups.class.php',
    'PagSeguroPaymentMethodName' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroAcceptedPaymentMethodName.class.php',
    'PagSeguroPaymentMethodType' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroPaymentMethodType.class.php',
    'PagSeguroPaymentMode' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroPaymentMode.class.php',
    'PagSeguroPaymentParser' => $baseDir . '/source/PagSeguroLibrary/parser/PagSeguroPaymentParser.class.php',
    'PagSeguroPaymentRequest' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroPaymentRequest.class.php',
    'PagSeguroPaymentService' => $baseDir . '/source/PagSeguroLibrary/service/PagSeguroPaymentService.class.php',
    'PagSeguroPhone' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroPhone.class.php',
    'PagSeguroPreApproval' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroPreApproval.class.php',
    'PagSeguroPreApprovalCharge' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroPreApprovalCharge.class.php',
    'PagSeguroPreApprovalParser' => $baseDir . '/source/PagSeguroLibrary/parser/PagSeguroPreApprovalParser.class.php',
    'PagSeguroPreApprovalRequest' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroPreApprovalRequest.class.php',
    'PagSeguroPreApprovalSearchResult' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroPreApprovalSearchResult.class.php',
    'PagSeguroPreApprovalSearchService' => $baseDir . '/source/PagSeguroLibrary/service/PagSeguroPreApprovalSearchService.class.php',
    'PagSeguroPreApprovalService' => $baseDir . '/source/PagSeguroLibrary/service/PagSeguroPreApprovalService.class.php',
    'PagSeguroPreApprovalStatus' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroPreApprovalStatus.class.php',
    'PagSeguroRefundParser' => $baseDir . '/source/PagSeguroLibrary/parser/PagSeguroRefundParser.class.php',
    'PagSeguroRefundService' => $baseDir . '/source/PagSeguroLibrary/service/PagSeguroRefundService.class.php',
    'PagSeguroRequest' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroRequest.class.php',
    'PagSeguroResources' => $baseDir . '/source/PagSeguroLibrary/resources/PagSeguroResources.class.php',
    'PagSeguroSearchResult' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroSearchResult.class.php',
    'PagSeguroSender' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroSender.class.php',
    'PagSeguroSenderDocument' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroSenderDocument.class.php',
    'PagSeguroServiceException' => $baseDir . '/source/PagSeguroLibrary/exception/PagSeguroServiceException.class.php',
    'PagSeguroServiceParser' => $baseDir . '/source/PagSeguroLibrary/parser/PagSeguroServiceParser.class.php',
    'PagSeguroSession' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroSession.class.php',
    'PagSeguroSessionParser' => $baseDir . '/source/PagSeguroLibrary/parser/PagSeguroSessionParser.class.php',
    'PagSeguroSessionService' => $baseDir . '/source/PagSeguroLibrary/service/PagSeguroSessionService.class.php',
    'PagSeguroShipping' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroShipping.class.php',
    'PagSeguroShippingType' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroShippingType.class.php',
    'PagSeguroTransaction' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroTransaction.class.php',
    'PagSeguroTransactionCancellationSource' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroTransactionCancellationSource.class.php',
    'PagSeguroTransactionCreditorFees' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroTransactionCreditorFees.class.php',
    'PagSeguroTransactionParser' => $baseDir . '/source/PagSeguroLibrary/parser/PagSeguroTransactionParser.class.php',
    'PagSeguroTransactionSearchResult' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroTransactionSearchResult.class.php',
    'PagSeguroTransactionSearchService' => $baseDir . '/source/PagSeguroLibrary/service/PagSeguroTransactionSearchService.class.php',
    'PagSeguroTransactionStatus' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroTransactionStatus.class.php',
    'PagSeguroTransactionSummary' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroTransactionSummary.class.php',
    'PagSeguroTransactionType' => $baseDir . '/source/PagSeguroLibrary/domain/PagSeguroTransactionType.class.php',
    'PagSeguroXmlParser' => $baseDir . '/source/PagSeguroLibrary/utils/PagSeguroXmlParser.class.php',
);
