
var varPagSeguro = {};
	varPagSeguro.btPagamento = $('[rel="btnPagamento"]');
	varPagSeguro.senderHash = '';
	varPagSeguro.ambiente = '';
	varPagSeguro.sandbox = '';
    varPagSeguro.sessionId = '';
    varPagSeguro.url = '';
    varPagSeguro.config = [];
    varPagSeguro.obj = [];


var PagSeguro_inc = {

    init: function(obj){

        varPagSeguro.config = obj.config;

        if(obj.config.sandbox){
        	varPagSeguro.ambiente = 'sandbox.';
        }else{
        	varPagSeguro.ambiente = '';
        }

        //varPagSeguro.url = 'gateways/pagseguro/criarSessao.php';
        varPagSeguro.obj = obj;

        PagSeguro_inc.sessionId();
        //PagSeguro_inc.getSenderHash();
    },

    getSenderHash: function(){
    	varPagSeguro.senderHash = PagSeguroDirectPayment.getSenderHash();
    	varFatura.post['senderHash'] = varPagSeguro.senderHash;
    },

    sessionId: function(){
    	var config = varPagSeguro.config;

    	if(varPagSeguro.ambiente == 'sandbox.'){
    		var sandbox = 1;
    	}else{
    		var sandbox = 0;
    	}

    	$.ajax({
			type: 'POST',
			url: 'gateways/pagseguro/criarSessao.php',
			data: {dados: {email: config.email, token: config.token, sandbox: sandbox}},
			dataType: 'html',
			success: function(data){
				varPagSeguro.sessionId = data;
				PagSeguroDirectPayment.setSessionId(data);
			}
		});
    },

    boletoDireto: function(config, post){
        
        $.ajax({
            type: 'POST',
            url: 'gateways/pagseguro/boletoDireto.php',
            data: {config: config, post: post},
            dataType: 'json',
            success: function(data){
                $('#loadingPagamento').slideUp('fast');
                $('#prePagamento').slideDown('fast');

                var json = data.retorno;
                window.open(json.link_pagamento, '_blank');
            }
        });
    }
}