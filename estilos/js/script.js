$(function(){
 	//Carregando estilos...
	$('head').loadCSS('estilos/css/bootstrap.min.css');
	$('head').loadCSS('estilos/css/fatura.css');
	$('head').loadCSS('estilos/css/fontello.css');

	$('body').loadJS('estilos/js/bootstrap.min.js');

	//Iniciando Widgets...
	

	linkBotao = function(data){
		$('.btnPagamento').attr("href", data.url);
	}

	linkFalha = function(data){
		console.log(data);
	}

	$('.selecionaPagto').live('click', function(){
		$('.formaPagamento').removeClass('formaPagamento_active');
    	$(this).addClass('formaPagamento_active');

		var gateway = $(this).children().find('[name="FORMA_PAGAMENTO"]');
		$('.E_PAGAMENTO').val(gateway.val());

		$(gateway).prop("checked", true);
	});	
	
	$('#bt_PagarFatura').live('click', function(){

		var gateway = $('.E_PAGAMENTO').val();
		var PagSeguro_sessionId = $('.PagSeguro_sessionId').val();
		var boletoGateway = $('[name="boletoGateway"]').val();

		if(PagSeguro_sessionId != null){
			PagSeguroDirectPayment.setSessionId(PagSeguro_sessionId);
	        var hashComprador = PagSeguroDirectPayment.getSenderHash();

	        $('.PagSeguro_senderHash').val(hashComprador);
		}

		$.ajax({
			type: 'POST',
			url: "./pagamento/",
			data: $('#efetuarPagamento').serialize(),
			beforeSend: function(){
				$('.tit_seleciona_pagamento, .formaPagamento, #bt_PagarFatura').hide();
				$('.loading_'+gateway).fadeIn();
			},
			success: function(data){
				$('.loading_'+gateway).slideUp();
				$('.return_'+gateway).slideDown();
				//var retorno = JSON.parse(data);

				if(gateway == "boleto"){

					console.log(data);

					switch(boletoGateway){
						case 'moip':

							if(retorno.Resposta != null){
								if(retorno.Resposta.Status == "Sucesso"){
									$('.return_'+gateway).fadeIn();
									$('#MoipWidget').attr("data-token", retorno.Resposta.Token);
								
					                var settings = {
					                    "Forma": "BoletoBancario"
					                }
					                MoipWidget(settings);
								}
								if(retorno.Resposta.Status == "Falha"){
									$('.error_'+gateway).fadeIn();
									$('.retornoFalha').html(retorno.Resposta.Erro);
								}
							}else{
								$('.return_'+gateway).fadeIn();
								$('.btnPagamento').attr("href", retorno.LINK_PAGAMENTO);
							}
							
							
						break;

						default:
							$('.btnPagamento').attr("href", data);
						break;
					}	
				}else{
					$('.btnPagamento').attr("href", data);
				}
			}
		});
	});

});

jQuery.fn.loadCSS = function(href){
	var cssLink = jQuery("<link rel='stylesheet' type='text/css' medial='all' href='"+ href +"'>");
    jQuery(this).append(cssLink); 
};

jQuery.fn.loadJS = function(href){
	var jsLink = jQuery("<script type='text/javascript' src='"+ href +"'>");
	jQuery(this).append(jsLink); 
};

