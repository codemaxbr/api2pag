jQuery.fn.loadCSS = function(href){
	var cssLink = jQuery("<link rel='stylesheet' type='text/css' medial='all' href='"+ href +"'>");
    jQuery(this).append(cssLink); 
};

jQuery.fn.loadJS = function(href){
	var jsLink = jQuery("<script type='text/javascript' src='"+ href +"'>");
	jQuery(this).append(jsLink); 
};

jQuery.fn.serializeObject = function(){
    var o = {};
    var a = this.serializeArray();

    jQuery.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });

    return o;
};

var varFatura = {};
	varFatura.btPagamento = $('[rel="btnPagamento"]');
	varFatura.post = [];
	varFatura.gateway = '';
	varFatura.formaPagamento = [];
	varFatura.config = [];
	varFatura.gatewaysAtivos = [];
	varFatura.boletoExiste = '';
	varFatura.linkPagamento = '';


var faturaExec = {

	gatewaysAtivos: function(){
		$.ajax({
			type: 'GET',
			url: './api/getGatewaysAtivos/json',
			//data: {dados: {email: config.email, token: config.token, sandbox: sandbox}},
			dataType: 'json',
			success: function(data){
				$(data.resultado).each(function(i, val){
					varFatura.gatewaysAtivos.push(val.gateway);
					varFatura.formaPagamento.push(val.gateway);

					faturaExec.getConfig(val.gateway);
				});
			}
		});
	},

	segundaVia: function(gateway, tipo, dados){
		var dadosFatura = dados;
		$.ajax({
			type: 'POST',
			url: 'api/buscaTransacao',
			data: {gateway: gateway, tipo: tipo, fatura: dadosFatura.faturaID},
			dataType: 'html',
			beforeSend: function(){
				$('#prePagamento').slideUp('fast');
				$('#loadingPagamento').slideDown('fast');
			},
			success: function(data){
				if(data == "gera"){
					faturaExec.geraBoleto(gateway, dadosFatura);
				}else{
					var boletoData = JSON.parse(data);
					varFatura.linkPagamento = boletoData.link_pagamento;
					window.open(varFatura.linkPagamento, '_blank');

					$('#loadingPagamento').slideUp('fast');
					$('#prePagamento').slideDown('fast');
				}
			}
		});
	},

	geraBoleto: function(gateway, dados){
		varFatura.post = dados;
		faturaExec.getConfig(gateway);

		switch(gateway){
			case 'pagseguro':
				console.log(dados);
				console.log(varFatura.config);
				/*
				PagSeguro_inc.getSenderHash();

				if(varPagSeguro.senderHash != 'undefined'){
					PagSeguro_inc.boletoDireto(varFatura.config, varFatura.post);
				}else{
					alert('Erro ao tentar gerar o senderHash');
				}
				*/
			break;

			case 'gerencianet':
				console.log(dados);
				console.log(varFatura.config);
			break;
		}
	},

	getConfig: function(gateway){

		$.ajax({
			type: 'POST',
			url: './fatura/initJs/'+ gateway,
			data: varFatura.post,
			beforeSend: function(){
				//console.log('carregando...');
			},
			success: function(data){
				var fatura = JSON.parse(data);

				varFatura.config = fatura.config;
				varFatura.gateway = fatura.gateway;

				faturaExec.initGateway(varFatura);
				
			}
		});
	},

	initGateway: function(obj){
		//console.log(obj);

		switch(obj.gateway){
			case 'pagseguro':
				PagSeguro_inc.init(obj);
			break;
		}
	}
}