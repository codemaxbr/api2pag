$(function(){
 	//Carregando estilos...
	$('head').loadCSS('estilos/css/bootstrap.min.css');
	$('head').loadCSS('estilos/css/fatura.css');
	$('head').loadCSS('estilos/css/fontello.css');

	$('body').loadJS('estilos/js/bootstrap.min.js');
	$('head').loadJS('estilos/js/jquery.cookie.js');

	//Iniciando Widgets...
	//faturaExec.gatewaysAtivos();
	
	varFatura.btPagamento.click(function(){
		var gateway = $(this).data('id');
		var dados 	= $('#dadosPagamento').serializeObject();

		if(gateway == "boleto"){
			var boleto = $(this).attr('boleto');
			faturaExec.segundaVia(boleto, 'boleto', dados);
		}
		
	});

});

