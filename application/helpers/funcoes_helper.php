<?php //if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    /*
    function dir_application(){
        return dirname(BASEPATH).'/application';
    }
    */

    /*   
    retorna o dominio de uma URL
     
    formata o caminho de uma URL
        dominio/pasta/dos/arquivos
        protocolo://dominio/pasta/dos/arquivos
    */

    function siteURL(){
        return 'http://localhost/api2pag';
    }
     
    function getDominio($url) {
        if ($url == '') return false;
        $url = explode('/', $url);
        if (($url[1] == '') && (count($url) > 2)) return $url[2];
        return $url[0];
    }

    function arrayToXML($data, $rootNodeName = 'resultado', $xml = null) {
         
        // desligamos essa opção para evitar bugs
        if (ini_get('zend.ze1_compatibility_mode') == 1) {
            ini_set('zend.ze1_compatibility_mode', 0);
        }
     
        if ($xml == null) {
            header("Content-Type: text/xml");            
            $xml = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><$rootNodeName />");
        }
     
        // faz o loop no array
        foreach ($data as $key => $value) {
            // se for indice numerico ele renomeia o indice
            if (is_numeric($key)) {
                $key = "item";
            }
     
            // substituir qualquer coisa não alfa numérico
            //$key = preg_replace('/[^a-z]/i', '', $key);
     
             
            if (is_array($value)) {
                $node = $xml->addChild($key);
                arrayToXML($value, $rootNodeName, $node);
            } else {
                $value = htmlentities($value);
                $xml->addChild($key, html_entity_decode($value));
            }
        }

        ob_clean();
        return $xml->asXML();
    }

    // function defination to convert array to xml
    function array_to_xml( $data, &$xml_data ) {
        foreach( $data as $key => $value ) {
            if( is_array($value) ) {
                if( is_numeric($key) ){
                    $key = 'item'.$key; //dealing with <0/>..<n/> issues
                }
                $subnode = $xml_data->addChild($key);
                array_to_xml($value, $subnode);
            } else {
                $xml_data->addChild("$key",htmlspecialchars("$value"));
            }
         }
    }

    function arquivoTamanho($size) {
        if (is_file($size)) {
            $size = filesize($size);
        }
        if ($size == 0) {
            $size = 1;
        }
        $filesizename = array("bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB");
        return round($size/pow(1000, ($i = floor(log($size, 1000)))), 2) . $filesizename[$i];
    }

    function uploadFile($arquivo, $pasta, $tipos, $nome = null){
        if(isset($arquivo)){
            $infos = explode(".", $arquivo["name"]);
     
            if(!$nome){
                for($i = 0; $i < count($infos) - 1; $i++){
                    @$nomeOriginal = $nomeOriginal . $infos[$i] . ".";
                }
            }
            else{
                $nomeOriginal = $nome . ".";
            }
     
            $tipoArquivo = $infos[count($infos) - 1];
     
            $tipoPermitido = false;
            foreach($tipos as $tipo){
                if(strtolower($tipoArquivo) == strtolower($tipo)){
                    $tipoPermitido = true;
                }
            }
            if(!$tipoPermitido){
                $retorno["erro"] = "Tipo não permitido";
            }
            else{
                if(move_uploaded_file($arquivo['tmp_name'], $pasta . $nomeOriginal . $tipoArquivo)){
                    //$retorno["caminho"] = $pasta . $nomeOriginal . $tipoArquivo;
                    $retorno["ext"] = $tipoArquivo;
                }
                else{
                    $retorno["erro"] = "Erro ao fazer upload";
                }
            }
        }
        else{
            $retorno["erro"] = "Arquivo nao setado";
        }
        return $retorno;
    }

    function telefoneDDD($telefone){
        return substr(limpaNumeros($telefone), 0, 2);
    }

    function limitarTexto($texto, $limite){
        $contador = strlen($texto);
        if ( $contador >= $limite ) {      
            $texto = substr($texto, 0, strrpos(substr($texto, 0, $limite), ' ')) . '...';
            return $texto;
        }
        else{
            return $texto;
        }
    }

    function limpaNumeros($numeros){
        $novo_numero = str_replace("(", "", $numeros);
        $novo_numero = str_replace(")", "", $novo_numero);
        $novo_numero = str_replace("-", "", $novo_numero);
        $novo_numero = str_replace(" ", "", $novo_numero);
        $novo_numero = str_replace(".", "", $novo_numero);
        $novo_numero = str_replace("/", "", $novo_numero);
        $novo_numero = str_replace("\/", "", $novo_numero);

        return $novo_numero;
    }

    function jsonPOST($url, $post){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);  
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
        $result = curl_exec($ch);
        curl_close($ch);  // Seems like good practice

        echo $result;
    }

    function xmlPOST($url, $post){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);  
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
        $result = curl_exec($ch);
        curl_close($ch);  // Seems like good practice

        $xml = simplexml_load_string($result);

        return $xml;
    }

    function postCURL($url, $post){
        //print_r(http_build_query($post));

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,0);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post);

        curl_setopt($curl, CURLOPT_URL, $url);  

        $result = curl_exec($curl);
        curl_close($curl);

        //$xml = simplexml_load_string($result);

        return $result;
    }

    function getJSON($url){
        // Get cURL resource
        $curl = curl_init();
        
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_URL, $url);

        $result = curl_exec($curl);
        if ($result == false) {
          echo "Não foi possível ler o XML.";
        }
        curl_close($curl);

        return $result;
    }

    function getXML($url){
        // Get cURL resource
        $curl = curl_init();
        
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_URL, $url);

        $result = curl_exec($curl);
        if ($result == false) {
          echo "Não foi possível ler o XML.";
        }
        curl_close($curl);
        $xml = simplexml_load_string($result);

        return $xml;
    }

    function atualizaArquivos($file) { // $file = include path 

        $url = $file;
        $zipFile = __DIR__."/../../update.zip"; // Local Zip File Path
        $zipResource = fopen($zipFile, "w+");
        // Get The Zip File From Server
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); 
        curl_setopt($ch, CURLOPT_FILE, $zipResource);
        $page = curl_exec($ch);
        if(!$page) {
         echo "Error :- ".curl_error($ch);
        }
        curl_close($ch);

        $zip = new ZipArchive;
        $res = $zip->open($zipFile);
        if ($res === TRUE) {
            $zip->extractTo(__DIR__."/../../");
            $zip->close();

            unlink($zipFile);
            return TRUE;
        } else {
            return FALSE;
        }

    }

    function buscarCEP($cep){
        $url = "https://viacep.com.br/ws/".$cep."/json/";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $json = curl_exec($ch);

        curl_close($ch);

        $resultado = json_decode($json);
        if(!isset($resultado->erro) && isset($resultado->cep)){
            return $resultado;
        }        
    }

    function mascara_string($mascara, $string){
       $string = str_replace(" ","",$string);
       for($i = 0; $i < strlen($string); $i++){
          $mascara[strpos($mascara,"#")] = $string[$i];
       }
       
       return $mascara;
    }

    function linkText($text){
        // The Regular Expression filter
        $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";

        // Check if there is a url in the text
        if(preg_match($reg_exUrl, $text, $url)) {

           // make the urls hyper links
           return preg_replace($reg_exUrl, "<a target='_blank' href='{$url[0]}'>{$url[0]}</a> ", $text);

        } else {

           // if no urls in the text just return the text
           return $text;
        }
    }

    function array_group_by($arr, $key_selector) {
        $result = array();
        foreach ($arr as $i) {
            $key = $key_selector[$i];
            $result[$key][] = $i;
        }
        return $result;
    }

    function geraCupom($largura = 10) {
        $caracteres = "ABCDEFGHIJKLMNOPQRSTUXYVWZ0123456789";
        $tamanho = strlen($caracteres);
     
        $result = "";
     
        for ($i = 0; $i < $largura; $i++) {
            $index = mt_rand(0, $tamanho - 1);
            $result .= $caracteres[$index];
        }
     
        return $result;
    }

    function passRandom($largura = 6) {
        $caracteres = "abcdefghijklmnopqrstuxyvwzABCDEFGHIJKLMNOPQRSTUXYVWZ0123456789,.@#$%&*";
        $tamanho = strlen($caracteres);
     
        $result = "";
     
        for ($i = 0; $i < $largura; $i++) {
            $index = mt_rand(0, $tamanho - 1);
            $result .= $caracteres[$index];
        }
     
        return $result;
    }

    function anti_injection($string) {
        $string = stripslashes($string);
        $string = strip_tags($string);
        $string = mysql_real_escape_string($string);
        return $string;
    }

    function cleanuserinput($dirty){
        if (get_magic_quotes_gpc()) {
            $clean = mysql_real_escape_string(stripslashes($dirty));
        }else{
            $clean = mysql_real_escape_string($dirty);
        }
        return $clean;
    }

    function userKey(){

        $array = explode('.', $_SERVER['HTTP_HOST']);
        return @$array[1];
    }

    function getVars(){
        if(isset($_GET)){
            foreach ($_GET as $key => $value){
                return $key;
            }
        }else{
            return NULL;
        }
    }

    function numFormat($number){
        //$number = str_replace("-", "", $number);
        return number_format(doubleval($number), 2, ',', '.');
    }

    function lucroNum($v_ant, $v_nov){
        $p_lucro = 0; // porcentagem de lucro

        if($v_ant != 0){
            $p_lucro = round((($v_nov / $v_ant) - 1) * 100, 2);
        }
        return @$p_lucro . "%";

    }

    function numFormat_US($number){
        return number_format($number, 2, '.', ',');
    }
    function dateFormat_extenso($data_completa){
        $semana = date('w', strtotime($data_completa));
        $data = date('D', strtotime($data_completa));
        $mes = date('M', strtotime($data_completa));
        $dia = date('d', strtotime($data_completa));
        $ano = date('Y', strtotime($data_completa));
        $hora = date('H:i', strtotime($data_completa));
        
        switch($semana) {
            case 0: $dia_semana = "Dom"; break;
            case 1: $dia_semana = "Seg"; break;
            case 2: $dia_semana = "Ter"; break;
            case 3: $dia_semana = "Qua"; break;
            case 4: $dia_semana = "Qui"; break;
            case 5: $dia_semana = "Sex"; break;
            case 6: $dia_semana = "Sab"; break;
        }
        
        $mes_extenso = array(
            'Jan' => 'Jan',
            'Feb' => 'Fev',
            'Mar' => 'Mar',
            'Apr' => 'Abr',
            'May' => 'Mai',
            'Jun' => 'Jun',
            'Jul' => 'Jul',
            'Aug' => 'Ago',
            'Nov' => 'Nov',
            'Sep' => 'Set',
            'Oct' => 'Out',
            'Dec' => 'Dez'
        );
        
        return "(".$dia_semana.", ".$dia." ".$mes_extenso[$mes] . ", $ano às ".$hora.")";
    }

    function dateFormat($data, $formato = "d/m/Y") {
        $date = new DateTime($data);
        return $date->format($formato);
    }

    function dateTimestamp($data){
        $d = str_replace('/', '-', $data);
        return date('Y-m-d', strtotime($d));
    }

    function convertSize($tipo, $numero){
        switch ($tipo) {
            case 'MB': return $numero."MB"; break;
            case 'GB': return substr($numero,0 , -3)."GB"; break;
        }
    }

    function somaData($data, $dias, $meses, $ano){

        $data = explode("/", $data);
        $resultadoData = date("d/m/Y", mktime(0, 0, 0, $data[1] + $meses, $data[0] + $dias, $data[2] + $ano));
        return $resultadoData;
    }

    function subtraiData_timestamp($data, $dias, $meses, $anos){
        $nova_data = new DateTime($data);
        $monta_interval = "P";
        $monta_interval .= ($dias != 0) ? $dias."D" : "";
        $monta_interval .= ($meses != 0) ? $meses."M" : "";
        $monta_interval .= ($anos != 0) ? $anos."Y" : "";

        $nova_data->sub(new DateInterval($monta_interval));
        return $nova_data->format('Y-m-d');
    }

    function somaData_timestamp($data, $dias, $meses, $anos){
        $nova_data = new DateTime($data);
        $monta_interval = "P";
        $monta_interval .= ($dias != 0) ? $dias."D" : "";
        $monta_interval .= ($meses != 0) ? $meses."M" : "";
        $monta_interval .= ($anos != 0) ? $anos."Y" : "";

        $nova_data->add(new DateInterval($monta_interval));
        return $nova_data->format('Y-m-d');
    }

    function lista_bancos(){
        $ci = & get_instance();
        $sql = $ci->db->get('bancos');
        
        if($sql->num_rows > 0){
            return $sql->result();
        }else{
            return NULL;
        }
    }

    function lista_estados(){
        $ci = & get_instance();
        $sql = $ci->db->get('estados');
        
        if($sql->num_rows > 0){
            return $sql->result();
        }else{
            return NULL;
        }
    }

    function getEstados($cod_estado){
        $ci = & get_instance();
        $ci->db->where('CODIGO', $cod_estado);
        $sql = $ci->db->get('estados');
        
        if($sql->num_rows > 0){
            return $sql->row('SIGLA');
        }else{
            return NULL;
        }
    }

    function getCidades($cod_cidade){
        $ci = & get_instance();
        $ci->db->where('CODIGO', $cod_cidade);
        $sql = $ci->db->get('cidades');
        
        if($sql->num_rows > 0){
            return $sql->row('DESCRICAO');
        }else{
            return NULL;
        }
    }

    function tipos_conta(){
        $ci = & get_instance();
        $sql = $ci->db->get('contas_tipo');
        
        if($sql->num_rows > 0){
            return $sql->result();
        }else{
            return NULL;
        }
    }

    function _send_mail($data,$htmlMail) {

        $mail = new PHPMailer();
        $mail->IsSMTP(); //Definimos que usaremos o protocolo SMTP para envio.

        $mail->SMTPAuth = true; //Habilitamos a autenticação do SMTP. (true ou false)
        $mail->Host = "mail.codemax.com.br"; //Podemos usar o servidor do gMail para enviar.
        $mail->Port = 587; //Estabelecemos a porta utilizada pelo servidor do gMail.
        $mail->Username = "lucas@codemax.com.br"; //Usuário do gMail
        $mail->Password = "lucm871g."; //Senha do gMail
        
        $mail->SetFrom('lucas@codemax.com.br', LICENSE_NAME); //Quem está enviando o e-mail.
        //$mail->AddReplyTo("response@email.com","Nome Completo"); //Para que a resposta será enviada.
        $mail->IsHTML(true); // send as HTML

        $mail->Subject = html_entity_decode($data['ASSUNTO_EMAIL']); //Assunto do e-mail.        
        
        $mail->Body = $htmlMail;
        $mail->AddAddress($data['EMAIL'], $data['NOME']);
   
        if(!$mail->Send()) {
            $message = "ocorreu um erro durante o envio: " . $mail->ErrorInfo;
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /*
    function subtraiData_timestamp($givendate,$day=0,$mth=0,$yr=0) {
        $cd = strtotime($givendate);
        $newdate = date('Y-m-d', mktime(date('h',$cd),
        date('i',$cd), date('s',$cd), date('m',$cd)-$mth,
        date('d',$cd)-$day, date('Y',$cd)-$yr));
        return $newdate;
    }
    */

    function mesAnterior($givendate,$day=0,$mth=1,$yr=0) {
        $cd = strtotime($givendate);
        $newdate = date('Y-m', mktime(date('h',$cd),
        date('i',$cd), date('s',$cd), date('m',$cd)-$mth,
        date('d',$cd)-$day, date('Y',$cd)-$yr));
        return $newdate;
    }

    /* Somar 45 dias ao dia 07/05/2012 */
    #echo somar_data('07/05/2012', 45, 0, 0);     Imprime 21/06/2012 
 
    /* Somar 6 meses ao dia 28/02/2012  */
    #echo somar_data('28/02/2012', 0, 6, 0);      Imprime 28/08/2012
 
    /* Somar 2 anos, 3 meses e 10 dias ao dia 01/01/2012 */
    #echo somar_data('01/01/2012', 10, 3, 2);     Imprime 11/04/2014

    function diferencaData($data){

        $inicio = date("Y-m-d", strtotime($data));
        $fim = date('Y-m-d');

        list($anoInicio, $mesInicio, $diaInicio) = explode('-', $inicio);
        list($anoFim, $mesFim, $diaFim) = explode('-', $fim);
        
        $dataInicio = mktime(0,0,0, $mesInicio, $diaInicio, $anoInicio);
        $dataFim    = mktime(0,0,0, $mesFim, $diaFim, $anoFim);
        
        $diferenca = ($dataInicio > $dataFim) ? $dataInicio - $dataFim : $dataFim - $dataInicio;

        return intval($diferenca / 86400);
    }

    function diferencaDias($dia){

        $time_inicial = strtotime($dia);
        $time_final = strtotime(date('Y-m-d'));
        // Calcula a diferença de segundos entre as duas datas:
        $diferenca = $time_final - $time_inicial; // 19522800 segundos
        // Calcula a diferença de dias
        return (int)floor( $diferenca / (60 * 60 * 24)); // 225 dias
    }

    //Agora chamamos a função, e passamos os valores para calcular
    #echo intervaloData('2010-12-05', '2010-12-30').' dias';

    function user_email($email){
        $array = explode('@', $email);
        return $array[0];
    }

    function limpaChars($string){
        $string = str_replace(" ", "_", $string);
        $string = str_replace(".", "_", $string);
        $string = str_replace("/", "-", $string);
        $string = str_replace("Á", "A", $string);
        $string = str_replace("á", "a", $string);
        $string = str_replace("ã", "a", $string);
        $string = str_replace("É", "E", $string);
        $string = str_replace("é", "e", $string);
        $string = str_replace("Í", "I", $string);
        $string = str_replace("í", "i", $string);
        $string = str_replace("Ó", "O", $string);
        $string = str_replace("ó", "o", $string);
        $string = str_replace("õ", "o", $string);
        $string = str_replace("Ú", "U", $string);
        $string = str_replace("ú", "u", $string);
        $string = str_replace("ç", "c", $string);
        $string = str_replace("Ç", "c", $string);

        return $string;
    }

    function limpaUser($string){
        $string = str_replace(" ", "_", $string);
        $string = str_replace(".", "", $string);
        $string = str_replace("/", "", $string);
        $string = str_replace("(", "", $string);
        $string = str_replace(")", "", $string);
        $string = str_replace("'", "", $string);
        $string = str_replace('"', "", $string);
        $string = str_replace(";", "", $string);
        $string = str_replace(";", "", $string);
        $string = str_replace(':', "", $string);
        $string = str_replace("Á", "A", $string);
        $string = str_replace("á", "a", $string);
        $string = str_replace("ã", "a", $string);
        $string = str_replace("É", "E", $string);
        $string = str_replace("é", "e", $string);
        $string = str_replace("Í", "I", $string);
        $string = str_replace("í", "i", $string);
        $string = str_replace("Ó", "O", $string);
        $string = str_replace("ó", "o", $string);
        $string = str_replace("õ", "o", $string);
        $string = str_replace("Ú", "U", $string);
        $string = str_replace("ú", "u", $string);
        $string = str_replace("ç", "c", $string);
        $string = str_replace("Ç", "c", $string);

        return $string;
    }

    function niceTime($input) {
        $minute = 60; // seconds
        $hour = 3600; // seconds
        $day = 86400; // seconds
        $week = 604800; // seconds
        $month = 2629743; // seconds
        $year = 31556926; // seconds
         
        $localtime = time();
        $time = strtotime($input); // transform input to timestamp
        $diff = $localtime - $time; // get difference in seconds
         
        switch (true) {
                      
            // seconds
            case ($diff < $minute); // if difference less them a minute
            $count = $diff; // hold the value
            if ($count == 0) {
                $count = 1;
                $suffix = "segundo";
            } elseif ($count == 1) {
                $suffix = "segundo";
            } else {
                $suffix = "segundos";
            }
            break;
             
            // minute
            case ($diff > $minute && $diff < $hour); /* if difference greater them a minute and if diff less them an hour */
            $count = floor($diff / $minute); // here i divided by minute in order to get 1
            if ($count == 1) {
                $suffix = "minuto";
            } else {
                $suffix = "minutos";
            }
            break;
 
            // hour
            case ($diff > $hour && $diff < $day); /* if difference greater them an hour and if diff less them a day */
            $count = floor($diff / $hour); // here i divided by hour in order to get 1
            if ($count == 1) {
                $suffix = "hora";
            } else {
                $suffix = "horas";
            }
            break;
             
            // day
            case ($diff > $day && $diff < $week); /* if difference greater them a day and if diff less them a week */
            $count = floor($diff / $day); // here i divided by day in order to get 1
            if ($count == 1) {
                $suffix = "dia";
            } else {
                $suffix = "dias";
            }
            break;
 
            // week
            case ($diff > $week && $diff < $month); /* if difference greater them a week and if diff less them a month */
            $count = floor($diff / $week); // here i divided by week in order to get 1
            if ($count == 1) {
                $suffix = "semana";
            } else {
                $suffix = "semanas";
            }
            break;
             
            // month
            case ($diff > $month && $diff < $year); /* if difference greater them a month and if diff less them a year */
            $count = floor($diff / $month); // here i divided by month in order to get 1
            if ($count == 1) {
                $suffix = "mês";
            } else {
                $suffix = "mêses";
            }
            break;
 
            // year
            case ($diff > $year); /* if difference greater them a year */
            $count = floor($diff / $year); // here i divided by year in order to get 1
            if ($count == 1) {
                $suffix = "ano";
            } else {
                $suffix = "anos";
            }
            break;
        }
        return $count." ".$suffix." atrás";
    }

    
?>