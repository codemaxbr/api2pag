<div id="top_header">
	<div class="handlers">
    	<p>ID FATURA</p>
        <b>#<?=$xml->fatura_id?></b>
    </div>
    <div class="handlers" style="border-right:0px;">
    	<p>VENCIMENTO</p>
        <b class="bold_valor"><?=dateFormat($xml->fatura_vencimento)?></b>
    </div>
    
    <div class="handlers" style="border-right:0px; margin:0px; float:right;">
    	<?php switch($xml->fatura_status){
			case 'nao pago': echo '<span class="label label-important">NÃO PAGO</span>'; break;
			case 'em aberto': echo '<span class="label label-warning">EM ABERTO</span>'; break;
			case 'pago': echo '<span class="label label-success">PAGO</span>'; break;
		}?>
    </div>
</div>