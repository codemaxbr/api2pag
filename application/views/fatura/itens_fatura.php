<div id="fatura_detalhe">
	<b class="itens_fatura">Itens da Fatura</b>
    
    <table class="table table-striped table-hover" id="tabela_faturaDetalhe" style="border:1px solid #eee;">
        <thead>
            <tr>
              <th width="80%">Detalhes do Serviço</th>
              <th width="5%"></th>
              <th width="15%">Valor</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($xml->fatura_itens as $item):?>
            <tr>
              <td>
              <?=$item->descricao?> <?=($item->inicio != '0000-00-00') ? "-  &nbsp; (".$item->inicio." até ".$item->vencimento.")" : ""?>
    		  <?=($item->dominio != "" ? '<br /><b style="line-height:10px;">'.$item->dominio."</b>" : "")?>
              </td>
              <td>( - )</td>
              <td>R$ <?=numFormat($item->preco)?></td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
    
    <div id="fatura_observacoes">
    	<b class="fatura_obs">Nota</b>
    	
        <p>Selecione uma forma de pagamento e clique no botão "<b>Pagar fatura</b>" abaixo.</p>
        <p>Após confirmação do pagamento efetuada automaticamente, os serviços contratados serão liberados.</p>
    </div>
    
    <div id="fatura_total">
        <table id="tabela_faturaTotal">
                
            <tr>
              <td>Sub-total</td>
              <td align="right"><b>R$ <?=numFormat($xml->fatura_valor)?></b></td>
            </tr>
            <tr>
              <td>Taxas / Créditos</td>
              <td align="right"><b>0,00</b></td>
            </tr>
            <tr>
              <td><b class="texto_total">Total a Pagar</b></td>
              <td align="right"><b class="total_valor">R$ <?=numFormat($xml->fatura_valor)?></b></td>                      
            </tr>
                
        </table>
    </div>
</div>