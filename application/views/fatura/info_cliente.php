<div id="fatura_cliente">
	<div style="float:left">
    	<b class="detalhe_cliente">Informações do Cliente</b>
    	<?=$xml->cliente->razao_nome?><br />
        <?=($xml->cliente->tipo_pessoa == "fisica") ? "CPF: ".$xml->cliente->cpf : "CNPJ: ".$xml->cliente->cnpj;?><br />
        <br />
        E-mail: <?=$xml->cliente->email?><br />
        Tel: <?=$xml->cliente->telefone?>
    </div>
    <div style="float:right; border:0px;">
    	<b class="detalhe_endereco">Dados de Endereço</b>
    	<?=$xml->cliente->logradouro?>, <?=$xml->cliente->numero?><br />
        <?=$xml->cliente->complemento?>, <?=$xml->cliente->bairro?><br />
        <br />
        <?=$xml->cliente->cidade?>, <?=$xml->cliente->uf?><br />
        CEP: <?=$xml->cliente->cep?><br />
    </div>
</div>