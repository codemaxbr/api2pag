<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Fatura #<?=$xml->fatura_id?></title>

<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8; IE=EmulateIE9; IE=EmulateIE10">
<!--<meta http-equiv="Access-Control-Allow-Origin" content="*">-->

<link rel="shortcut icon" href="estilos/favicon.ico" />
<link rel="icon" type="image/gif" href="estilos/favicon.ico" />

<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800'>
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext">
<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>
</head>

<body id="fatura">
    <!-- Vencimento dias antes-->
    <div class="alert alert-darkblue faturaNotes text-center" role="alert">
        Esta fatura vence dia <b>31/03/2016</b> e está disponível para pagamento.
    </div>
    

    <!-- Vencimento amanhã 
    <div class="alert alert-darkorange faturaNotes text-center" role="alert">
        Esta fatura vence <b>AMANHÃ</b>. Evite a suspensão do seu serviço.
    </div>
    -->

    <!-- Vencimento hoje 
    <div class="alert alert-darkorange faturaNotes text-center" role="alert">
        Esta fatura vence <b>HOJE</b>. Favor efetuar o pagamento mais breve possível.
    </div>
    -->

    <!-- Fatura atrasada 
    <div class="alert alert-darkred faturaNotes text-center" role="alert">
        Esta fatura está atrasada em <b>2 dias</b>. Favor efetuar o pagamento mais breve possível.
    </div>
    -->

    <!-- Aviso de Suspensão 
    <div class="alert alert-darkblack faturaNotes text-center" role="alert">
        <b>Aviso de Suspensão!</b> Serviço suspenso por falta de pagamento.
    </div>
    -->

    <form id="dadosPagamento">
    <?php
        $data = array(
            'contaId' => $xml->conta_id,
            'faturaID'  => $xml->fatura_id,
            'Descricao' => 'Fatura #'.$xml->fatura_id.' - ',
            'Valor'   => $xml->fatura_valor,
            'Vencimento'   => $xml->fatura_vencimento,
            'Data'   => $xml->fatura_emissao,
            'clientePessoa' => $xml->cliente->tipo_pessoa,
            'clienteID'  => $xml->cliente->cliente_id,
            'clienteNome'  => $xml->cliente->razao_nome,
            'clienteCPF' => $xml->cliente->cpf,
            'clienteCNPJ'   => $xml->cliente->cnpj,
            'clienteDDD'  => ($xml->cliente->telefone) ? substr(limpaNumeros($xml->cliente->telefone), 0, 2) : substr(limpaNumeros($xml->cliente->celular), 0, 2),
            'clienteTel' => ($xml->cliente->telefone) ? substr(limpaNumeros($xml->cliente->telefone), 2) : substr(limpaNumeros($xml->cliente->celular), 2),
            'clienteEmail' => $xml->cliente->email,
            'clienteLogradouro'  => $xml->cliente->logradouro,
            'clienteNumero' => $xml->cliente->numero,
            'clienteComplemento'   => $xml->cliente->complemento,
            'clienteBairro'  => $xml->cliente->bairro,
            'clienteCEP' => limpaNumeros($xml->cliente->cep),
            'clienteCidade'   => $xml->cliente->cidade,
            'clienteUF'  => $xml->cliente->uf
        );

        echo form_hidden($data);
    ?>
    </form>
    
    <!-- Header -->
	<div class="container">
        <div class="col-md-3 noPadding marginTop_15p pagtoFatura">
            <p>ID FATURA</p>
            <h1>#1000000054</h1>
        </div>
        <div class="col-md-9 noPadding text-right marginTop_15p pagtoFatura">
            <div id="loadingPagamento" style="display: none;">
                <div class="col-md-6 pull-right divLoading">
                    <img src="estilos/images/loading/loading4.gif" class="pull-left" />
                    <div class="clearfix">Iniciando processo de pagamento, aguarde.</div>
                </div>
            </div>

            <div id="prePagamento">
                <p>Pagar com</p>
                <!--
            	<a rel="btnPagamento" data-id="gerencianet" class="btn btn-default">
                    <img src="estilos/images/gateways/gerencianet.png">
                </a>

                <a rel="btnPagamento" data-id="moip" class="btn btn-default">
                    <img src="estilos/images/gateways/moip.png">
                </a>
                -->

            	<a rel="btnPagamento" data-id="paypal" class="btn btn-default">
                    <img src="estilos/images/gateways/paypal.svg" style="height:22px; margin:4px 0;">
                </a>

                <a rel="btnPagamento" data-id="pagseguro" class="btn btn-default">
                    <img src="estilos/images/gateways/pagseguro.png" style="height:22px; margin:4px 0;">
                </a>

                <a rel="btnPagamento" data-id="boleto" boleto="pagseguro" class="btn btn-default">
                    <img src="estilos/images/gateways/icon_boleto.png">
                </a>

                <p>Se preferir, você pode efetuar o pagamento via Depósito Bancária</p>
            </div>
        </div>
    </div>
    <!-- fim Header -->

    <!-- Info Cliente -->
    <div class="container cotentFatura">
        <div class="row rowContent">
            <div class="col-md-6">
                <div class="col-md-12 noPadding marginTop_10p">
                    <img src="estilos/logos/codemax.png" class="logoOwner">
                </div>

                <div class="col-md-12 noPadding marginTop_20p infoEmpresa">
                    <b>Codemax Sistemas Ltda</b><br />
                    <p>CNPJ: 11.763.944/0001-37</p>
                    <p>financeiro@codemax.com.br</p>
                </div>
            </div>

            <div class="col-md-6 infoCliente marginTop_20p">
                <b>Cliente ID: #2040454</b>
                <p>
                    Lucas Maia de Paula<br />
                    CPF: 139.389.857-20
                </p>
                <p>
                    Rua Antonieta Rodrigues Vianna, 282 - B<br />
                    Itaboraí, RJ - CEP 24.858-564
                </p>
                <p>
                    lucas.codemax@gmail.com
                </p>
            </div>
        </div>

        <div class="row rowContent">
            <!-- Itens da Fatura -->
            <div class="col-md-12 itensFatura">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th class="col-md-9 text-left">Descrição</th>
                            <th class="col-md-1 text-center">Qtd</th>
                            <th class="col-md-2 text-right">Preço</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <b>Hospedagem de Sites - Nitro 5GB</b><br />
                                <p>
                                    <i>codemax.com.br</i><br />
                                    De 30/03/2016 até 30/04/2016
                                </p>
                            </td>
                            <td valign="middle" class="text-center">1</td>
                            <td valign="middle" class="text-right"><b>R$ 9.999,99</b></td>
                        </tr>
                  </tbody>
                </table>
            </div>

            <div class="col-md-12 importanteFatura">
                <div class="col-md-8 notaFatura">
                    <b>Atenção!</b>
                    <li>Após a data de vencimento, você deve efetuar o pagamento em até <strong>7 dias</strong>, caso contrário o serviço será suspenso.</li>
                    <li>Após confirmação do pagamento efetuada automaticamente, os serviços contratados serão liberados.</li>
                </div>

                <div class="col-md-4 totalFatura">
                    <div class="col-md-12 noPadding subtotalFatura">
                        <div class="col-md-5 noPadding">
                            Subtotal
                        </div>
                        <div class="col-md-7 noPadding text-right">
                            R$ 9.999,99
                        </div>
                    </div>

                    <div class="col-md-12 noPadding descontoFatura">
                        <div class="col-md-5 noPadding">
                            Desconto
                        </div>
                        <div class="col-md-7 noPadding text-right">
                            R$ 0,00
                        </div>
                    </div>

                    <div class="col-md-12 noPadding totalsFatura">
                        <div class="col-md-4 noPadding">
                            Total
                        </div>
                        <div class="col-md-8 noPadding text-right">
                            R$ 9.999,99
                        </div>
                    </div>
                </div>
            </div>
            <!-- fim Itens da Fatura -->            
        </div>
    </div>
    <!-- fim Info Cliente -->

    <div class="container footerFatura">
        <div class="col-md-12 text-center">
            <p>Esta fatura foi gerada com o <a href="#">GerentePRO</a></p>
        </div>
    </div>

    <script type="text/javascript" src="estilos/js/jquery.js"></script>
    <script type="text/javascript" src="estilos/js/app.js"></script>
    <script type="text/javascript" src="estilos/js/fatura.js"></script>
    <script type="text/javascript" src="estilos/js/gateways/pagseguro.js"></script>

    <script type="text/javascript" src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>
</body>
</html>