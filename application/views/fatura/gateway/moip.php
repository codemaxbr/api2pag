<!-- Pagamento por Moip -->
<div class="row formaPagamento selecionaPagto">
    <div class="span1 radio_selectPagamento" style="padding: 8px 4px;">
        <input type="radio" name="FORMA_PAGAMENTO" value="moip" class="" />
    </div>
    <div class="span3 img_selectPagamento">
        <img src="<?=URL_ASSETS?>/images/gateways/moip.png">
    </div>
    <div class="span7 txt_descricaoPagamento">
        <b>Pagar com Moip</b>
        Utilizando o Moip você poderá escolher entre diversas formas de pagamento abaixo:<br />
        <img src="<?=URL_ASSETS?>/images/gateways/pagseguro_cartoes.png">
    </div>
</div>