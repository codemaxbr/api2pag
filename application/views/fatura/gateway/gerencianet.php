<!-- Pagamento por GerenciaNet -->
<div class="row formaPagamento selecionaPagto">
    <div class="span1 radio_selectPagamento" style="padding: 8px 4px;">
        <input type="radio" name="FORMA_PAGAMENTO" value="gerencianet" class="" />
    </div>
    <div class="span3 img_selectPagamento">
        <img src="<?=URL_ASSETS?>/images/gateways/gerencianet.png">
    </div>
    <div class="span7 txt_descricaoPagamento">
        <b>Pagar com GerenciaNet</b>
        Utilizando o GerenciaNet você poderá escolher entre diversas formas de pagamento abaixo:<br />
        <img src="<?=URL_ASSETS?>/images/gateways/pagseguro_cartoes.png">
    </div>
</div>