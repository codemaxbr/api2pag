<!-- Pagamento por TrayCheckout -->
<div class="row formaPagamento selecionaPagto">
    <div class="span1 radio_selectPagamento" style="padding: 18px 4px;">
        <input type="radio" name="FORMA_PAGAMENTO" value="traycheckout" class="" />
    </div>
    <div class="span3 img_selectPagamento">
        <img src="<?=URL_ASSETS?>/images/gateways/traycheckout.svg" height="55">
    </div>
    <div class="span7 txt_descricaoPagamento">
        <b>Pagar com TrayCheckout</b>
        Utilizando o TrayCheckout você poderá escolher entre diversas formas de pagamento abaixo:<br />
        <img src="<?=URL_ASSETS?>/images/gateways/pagseguro_cartoes.png">
    </div>
</div>