<!-- Pagamento por Boleto -->
<div class="row formaPagamento selecionaPagto">
    <div class="span1 radio_selectPagamento">
        <input type="radio" name="FORMA_PAGAMENTO" value="boleto" class="" />
    </div>
    <div class="span3 img_selectPagamento">
        <img src="<?=URL_ASSETS?>/images/gateways/boleto.png">
    </div>
    <div class="span7 txt_descricaoPagamento">
        <b>Pagar com Boleto</b>
        Pague pelo seu Internet Banking, copiando o código de barras ou imprima o boleto e pague em caixas eletrônicos, bancos e casas lotéricas.
    </div>
</div>

<!-- Loading -->
<div class="row loading_gateway loading_boleto" style="display: none;">
    <div class="span7 txt_descricaoPagamento" style="display: block; text-align: center; margin-top: 10px;">
        <img src="<?=URL_ASSETS?>/img/gif/ip.gif">
        <b>Aguarde enquanto geramos o seu boleto</b>
    </div>
</div>

<!-- Retorno -->
<div class="row return_gateway return_boleto" style="display: none;">
    <div class="span7 txt_descricaoPagamento" style="display: block; text-align: center; margin-top: 10px;">
        <img src="<?=URL_ASSETS?>/images/gateways/boleto.png">
        
        Você deve efetuar o pagamento do boleto até a data de vencimento. <br />
        <strong>Seu pagamento será identificado em até 2 dias úteis.</strong>

        <div style="display: block; text-align: center; margin-top: 10px;">
            <a class="btn btn-inverse btn-large btnPagamento" target="_blank" style="text-decoration: none;">
                Imprimir Boleto
            </a>
        </div>
    </div>
</div>

<!-- Erro -->
<div class="row error_gateway error_boleto" style="display: none;">
    <div class="span7 txt_erroPagamento alert alert-danger">
        <img src="<?=URL_ASSETS?>/img/ico_erro.png">
        <font class="retornoFalha"></font>
    </div>
</div>