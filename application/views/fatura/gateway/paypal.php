<!-- Pagamento por PayPal -->
<div class="row formaPagamento selecionaPagto">
    <div class="span1 radio_selectPagamento" style="padding: 8px 4px;">
        <input type="radio" name="FORMA_PAGAMENTO" value="paypal" class="" />
    </div>
    <div class="span3 img_selectPagamento">
        <img src="<?=URL_ASSETS?>/images/gateways/paypal.svg" height="35">
    </div>
    <div class="span7 txt_descricaoPagamento">
        <b>Pagar com PayPal</b>
        Utilizando o PayPal você poderá escolher entre diversas formas de pagamento abaixo:<br />
        <img src="<?=URL_ASSETS?>/images/gateways/paypal_cartoes.png" height="80">
    </div>
</div>