<!-- Pagamento por PagSeguro -->
<div class="row formaPagamento selecionaPagto">
    <div class="span1 radio_selectPagamento" style="padding: 8px 4px;">
        <input type="radio" name="FORMA_PAGAMENTO" value="pagseguro" class="" />
    </div>
    <div class="span3 img_selectPagamento">
        <img src="<?=URL_ASSETS?>/images/gateways/pagseguro.png">
    </div>
    <div class="span7 txt_descricaoPagamento">
        <b>Pagar com PagSeguro</b>
        Utilizando o PagSeguro você poderá escolher entre diversas formas de pagamento abaixo:<br />
        <img src="<?=URL_ASSETS?>/images/gateways/pagseguro_cartoes.png">
    </div>
</div>

<!-- Loading -->
<div class="row loading_gateway loading_pagseguro" style="display: none;">
    <div class="span7 txt_descricaoPagamento" style="display: block; text-align: center; margin-top: 10px;">
        <img src="<?=URL_ASSETS?>/img/gif/ip.gif">
        <b>Aguarde enquanto enviamos sua solicitação para o PagSeguro</b>
    </div>
</div>

<!-- Retorno -->
<div class="row return_gateway return_pagseguro" style="display: none;">
    <div class="span7 txt_descricaoPagamento" style="display: block; text-align: center; margin-top: 10px;">
        <img src="<?=URL_ASSETS?>/images/gateways/pagseguro.png">
        
        Você será redirecionado para a página de pagamento do PagSeguro<br />
        <strong>Após finalizar o seu pagamento, a confirmação será feita automaticamente</strong>

        <div style="display: block; text-align: center; margin-top: 10px;">
            <a class="btn btn-inverse btn-large btnPagamento" target="_blank" style="text-decoration: none;">
                Ir para o PagSeguro
            </a>
        </div>
    </div>
</div>