<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contas_model extends CI_Model{

	public $contaID;

    public function __construct(){
        parent::__construct();
    }

    public function count_all($conta_id){
        $this->db->where('conta_id', $conta_id);
        return $this->db->count_all('contas');
    }

    public function getTodos($conta_id){

        $sql = $this->db->get('contas');
        if($sql->num_rows > 0){
            return $sql->result();
        }else{
            return NULL;
        }
    }

    public function atualizar($id, $dados){

        $this->db->where('CODIGO', $id);
        $sql = $this->db->update('contas', $dados);
        
        if($sql) return TRUE;    
    }
    
    public function excluir($auth){
        $this->db->where('auth', $auth);
        
        if($this->db->delete('contas')){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public function novo($post){
       
        if(!empty($post)){
            if($this->db->insert('contas', $post)){
                return $this->db->insert_id();
            }else{
                return FALSE;
            }            
        }
    }

    public function verificaExiste($auth){
        $this->db->select('acc.auth');
        $this->db->where('acc.auth', $auth);
        $this->db->from('contas as acc');

        $sql = $this->db->get();

        if($sql->num_rows > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function getConta($auth){
        $this->db->select('*');
        $this->db->where('acc.auth', $auth);
        $this->db->from('contas as acc');

        $sql = $this->db->get();

        if($sql->num_rows > 0){
            return $sql->row();
        }else{
            return NULL;
        }
    }
}

?>