<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Temas_model extends CI_Model{
    
    public $xml;
    public $tab = 0;
    
    
    public function __construct($version = '1.0', $encode = 'UTF-8'){
        parent::__construct();
    }
    
    public function openTag($name){
        $this->addTab();
        $this->xml .= "<".$name.">\n";
        $this->tab++;
    }   
}

?>