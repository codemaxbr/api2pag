<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Faturas_model extends CI_Model{

	private $cliente 		= NULL;
	private $valor 			= NULL;
	private $forma_pagto 	= NULL;
	private $ciclo_pagto 	= NULL;
	private $dominio 		= NULL;
	private $vale_desconto 	= NULL;
	private $ip 			= NULL;

	public $userKey;

    public function __construct(){
        parent::__construct();
    }


    public function delete($id){
        $this->db->where('CODIGO', $id);

        if($this->db->delete('faturas')){
            return TRUE;
        } else{
            return NULL;
        }
    }

    public function getMesAtual(){
        $this->db->order_by('STATUS ASC, CODIGO DESC'); 
        $this->db->like('VENCIMENTO', date("Y-m"));

        $sql = $this->db->get('faturas');
        if($sql->num_rows > 0){
            return $sql->result();
        }else{
            return NULL;
        }    
    }

    public function getMesAnterior(){
        $this->db->order_by('STATUS ASC, CODIGO DESC'); 
        $this->db->like('VENCIMENTO', mesAnterior(date("Y-m")));

        $sql = $this->db->get('faturas');
        if($sql->num_rows > 0){
            return $sql->result();
        }else{
            return NULL;
        }    
    }

    public function getAll(){
        //$this->db->order_by('CODIGO', 'DESC');
        //$this->db->order_by('STATUS', 'ASC');
        $this->db->order_by('STATUS ASC, CODIGO DESC'); 

        $sql = $this->db->get('faturas');
        if($sql->num_rows > 0){
            return $sql->result();
        }else{
            return NULL;
        }
    }

    public function count_byCliente($id_cliente){
        $this->db->select('COUNT(f.CLIENTE)');
        $this->db->from('faturas as f');

        $this->db->where('f.CLIENTE', $id_cliente);
        $sql = $this->db->get();

        return $sql->num_rows;
    }

    public function getApi_allCliente($id_cliente){
    	$this->db->select('f.CODIGO as FATURA_ID');
        $this->db->select('f.VALOR as FATURA_VALOR');
        $this->db->select('f.DATA as FATURA_EMISSAO');
        $this->db->select('f.TIPO as FATURA_TIPO');
        $this->db->select('f.VENCIMENTO as FATURA_VENCIMENTO');
        $this->db->select('f.HASH as FATURA_HASH');
        $this->db->select('f.DATA_UPDATE as FATURA_UPDATE');
        $this->db->select('f.STATUS as FATURA_STATUS');
        $this->db->select('f.CLIENTE as CLIENTE');
 
        $this->db->from('faturas as f');

        $this->db->order_by('f.STATUS ASC, f.CODIGO DESC');
        $this->db->where('f.CLIENTE', $id_cliente);
        $sql = $this->db->get();

        if($sql->num_rows > 0){
            return $sql->result();
        }else{
            return NULL;
        }
    }

    public function getPago(){
        $this->db->where_in('STATUS', 'pago');
        $sql = $this->db->get('faturas');
        if($sql->num_rows > 0){
            return $sql->result();
        }else{
            return NULL;
        }
    }

    public function get_NaoPago(){
        $this->db->where_not_in('STATUS', array('pago'));

        $sql = $this->db->get('faturas');
        if($sql->num_rows > 0){
            return $sql->result();
        }else{
            return NULL;
        }
    }

    public function getById($id = '', $hash = ''){
        $this->db->select('f.id as fatura_id');
        $this->db->select('f.valor as fatura_valor');
        $this->db->select('f.data as fatura_emissao');
        $this->db->select('f.data_update as fatura_update');
        $this->db->select('f.tipo as fatura_tipo');
        $this->db->select('f.vencimento as fatura_vencimento');
        $this->db->select('f.hash as fatura_hash');
        $this->db->select('f.status as fatura_status');
        $this->db->select('f.conta_id as conta');

        $this->db->select('c.id as cliente_id');
        $this->db->select('c.tipo_pessoa as cliente_pessoa');
        $this->db->select('c.razao_nome as cliente_nome');
        $this->db->select('c.cnpj as cliente_cnpj');
        $this->db->select('c.cpf as cliente_cpf');
        $this->db->select('c.data_nascimento as cliente_nascimento');
        $this->db->select('c.inscricao_municipal as cliente_inscricao_municipal');
        $this->db->select('c.inscricao_estadual as cliente_inscricao_estadual');
        $this->db->select('c.email as cliente_email');
        $this->db->select('c.email_cobranca as cliente_email_cobranca');
        $this->db->select('c.telefone as cliente_telefone');
        $this->db->select('c.celular as cliente_celular');
        $this->db->select('c.cep as cliente_cep');
        $this->db->select('c.logradouro as cliente_logradouro');
        $this->db->select('c.numero as cliente_numero');
        $this->db->select('c.complemento as cliente_complemento');
        $this->db->select('c.uf as cliente_uf');
        $this->db->select('c.cidade as cliente_cidade');
        $this->db->select('c.bairro as cliente_bairro');
        $this->db->select('c.responsavel as cliente_responsavel');
        $this->db->select('c.responsavel_cpf as cliente_responsavel_cpf');

        $this->db->select('acc.nome as titular');
        $this->db->select('acc.email as titular_email');

        $this->db->from('faturas as f');
        $this->db->join('clientes as c', 'c.id = f.cliente_id');
        $this->db->join('contas as acc', 'acc.id = f.conta_id');

        if($id != '') $this->db->where('f.id', $id);
        if($hash != '') $this->db->or_where('f.hash', $hash);
        $sql = $this->db->get();

        if($sql->num_rows > 0){
            return $sql->row();
        }else{
            return NULL;
        }
    }

    public function getItens($id){
        $this->db->select('i.produto_id as id');
        $this->db->select('i.produto_dominio as dominio');
        $this->db->select('i.produto_descricao as descricao');
        $this->db->select('i.produto_preco as preco');
        $this->db->select('i.produto_ciclo as ciclo');
        $this->db->select('i.data_inicio as inicio');
        $this->db->select('i.data_final as final');

        $this->db->from('faturas_itens as i');

        $this->db->where('i.fatura_id', $id);
        $sql = $this->db->get();

        if($sql->num_rows > 0){
            return $sql->result();
        }else{
            return NULL;
        }
    }

    public function haveToday($date){
        $this->db->like('DATA', $date);

        $sql = $this->db->get('faturas');
        if($sql->num_rows > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function dataVencimento($codigo){
        $this->db->where('CODIGO', $codigo);

        $sql = $this->db->get('faturas');
        if($sql->num_rows > 0){
            return $sql->row('VENCIMENTO');
        }else{
            return FALSE;
        }
    }

    public function thisUser($codigo, $status = array()){

        $this->db->where('CLIENTE', $codigo);
        if($status != NULL){
            $this->db->where_in('STATUS', $status);
        }

        $this->db->order_by('DATA', 'DESC');
        
        $sql = $this->db->get('faturas');
        if($sql->num_rows > 0){
            return $sql->result();
        }else{
            return NULL;
        }
    }
	
	public function setFatura($dados = array()){
        //Setando Pedido
        $add = array(
            'cliente_id' 	=> $dados['faturaCliente'],
	    	'valor' 		=> $dados['faturaValor'],
	    	'vencimento' 	=> $dados['faturaVencimento'],
            'status'        => 'em aberto',
            'tipo'          => $dados['faturaTipo'],
            'conta_id'      => $dados['contaID'],
        );
        
        if(!$this->db->insert('faturas', $add)){
            $this->db->_error_message();
        }

        $fatura_id = $this->db->insert_id();
        
        $set['hash'] = strtoupper(md5("fatura".$fatura_id));
        $this->db->where('id', $fatura_id);
        $this->db->update('faturas', $set);

        foreach ($dados['faturaItens'] as $item){
            switch($item['produtoCiclo']) {
                case 'mensal': $data_final = somaData_timestamp(date('Y-m-d'), 0, 1, 0); break;
                case 'bimestral': $data_final = somaData_timestamp(date('Y-m-d'), 0, 2, 0); break;
                case 'trimestral': $data_final = somaData_timestamp(date('Y-m-d'), 0, 3, 0); break;
                case 'semestral': $data_final = somaData_timestamp(date('Y-m-d'), 0, 6, 0); break;
                case 'anual': $data_final = somaData_timestamp(date('Y-m-d'), 0, 0, 1); break;
                case 'bienal': $data_final = somaData_timestamp(date('Y-m-d'), 0, 0, 2); break;
                case 'trienal': $data_final = somaData_timestamp(date('Y-m-d'), 0, 0, 3); break;
            }
            
            $add_item = array(
                'fatura_id'          => $fatura_id,
                'produto_id'         => $item['produtoId'],
                'produto_descricao'  => $item['produtoDescricao'],
                'produto_preco'      => $item['produtoPreco'],
                'produto_dominio'    => $item['produtoDominio'],
                'produto_qtd'        => $item['produtoQtd'],
                'produto_ciclo'      => $item['produtoCiclo'],
                'data_inicio'        => date('Y-m-d'),
                'data_final'         => $data_final
            );

            $this->db->insert('faturas_itens', $add_item);
        }

        return array('n_fatura' => $fatura_id, 'cliente' => $dados['faturaCliente']);        
    }

    public function novoStatus($status, $fatura){
        $this->db->where('CODIGO', $fatura);

    	$data['STATUS'] = $status;
    	$this->db->update('faturas', $data);
    }

    public function getStatus($fatura){
    	$this->db->where('FATURA', $fatura);
    	$sql = $this->db->get('faturas');

        if($sql->num_rows > 0){
            return $sql->row('STATUS');
        }else{
	    	return NULL;
		}
    }

    public function statusFatura($fatura){
        $this->db->where('CODIGO', $fatura);
        $sql = $this->db->get('faturas');

        if($sql->num_rows > 0){
            return $sql->row();
        }else{
            return NULL;
        }
    }
}

?>