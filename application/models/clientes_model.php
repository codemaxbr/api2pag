<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Clientes_model extends CI_Model{

	public $contaID;

    public function __construct(){
        parent::__construct();
    }

    public function count_all($conta_id){
        $this->db->where('conta_id', $conta_id);
        return $this->db->count_all('clientes');
    }

    public function count_id($id_cliente){
        $this->db->select('c.id');
        $this->db->from('clientes as c');

        $this->db->where('c.id', $id_cliente);
        $sql = $this->db->get();

        return $sql->num_rows;
    }

    public function getTodos($conta_id){

        $sql = $this->db->get('clientes');
        if($sql->num_rows > 0){
            return $sql->result();
        }else{
            return NULL;
        }
    }

    public function atualizar($id, $dados){

        $this->db->where('CODIGO', $id);
        $sql = $this->db->update('clientes', $dados);
        
        if($sql) return TRUE;    
    }
    
    public function excluir($id){
        $this->db->where('id', $id);
        if($this->db->delete('clientes')){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public function novo($dados){
       
        if(!empty($dados)){

            if($this->db->insert('clientes', $dados)){
                return $this->db->insert_id();
            }else{
                return FALSE;
            }
            
        }
        
    }

    public function getById($id){
        $this->db->where('id', $id);
        //$sql = $this->db->query('SELECT * FROM clientes WHERE CODIGO= "'.$id.'"');
        
        $sql = $this->db->get('clientes');

        if($sql->num_rows > 0){
            return $sql->row();
        }else{
            return NULL;
        }
    }
}

?>