<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gateways_model extends CI_Model{
    
    public $xml;
    public $tab = 0;
    
    
    public function __construct($version = '1.0', $encode = 'UTF-8'){
        parent::__construct();
    }

    public function addTransacao($dados){
        if(!$this->db->insert('transacoes', $dados)){
            return FALSE;
        }else{
            return TRUE;
        }
    }

    public function verificaTransacao($dados){
        $this->db->where('gateway', $dados['gateway']);
        $this->db->where('fatura', $dados['fatura']);
        $this->db->where('forma_pagamento', $dados['tipo']);

        $sql = $this->db->get('transacoes');

        if($sql->num_rows > 0){
            return $sql->row();
        }else{
            return FALSE;
        }
    }
    
    public function novoConfig($gateway, $conta, $config){
        $add['gateway'] = $gateway;
        $add['conta_id'] = $conta;
        $add['config'] = json_encode($config);

        if(!$this->db->insert('gateways', $add)){
            return FALSE;
        }else{
            return TRUE;
        }
    }

    public function gatewaysAtivos($conta){
        $this->db->select('gat.gateway');
        $this->db->select('gat.config');
        //$this->db->where('gat.conta_id', $conta);

        $this->db->from('gateways as gat');

        $this->db->join('contas as acc', 'acc.id = gat.conta_id', 'LEFT');

        if($conta != '') $this->db->where('acc.auth', $conta);
        if($conta != '') $this->db->or_where('acc.id', $conta);

        $sql = $this->db->get();

        if($sql->num_rows > 0){
            return $sql->result();
        }else{
            return NULL;
        }
    }

    public function getConfig($conta, $gateway){
        $this->db->select('gat.gateway');
        $this->db->select('gat.config');

        $this->db->select('acc.nome as titular');
        $this->db->select('acc.email as titular_email');

        $this->db->from('gateways as gat');
        $this->db->join('contas as acc', 'acc.id = gat.conta_id');

        if($conta != '') $this->db->where('acc.auth', $conta);
        if($conta != '') $this->db->or_where('acc.id', $conta);

        $this->db->where('gat.gateway', $gateway);

        $sql = $this->db->get();

        if($sql->num_rows > 0){
            return json_decode($sql->row('config'));
        }else{
            return NULL;
        }
    }
}

?>