<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fatura extends CI_Controller{

	public $id_fatura;
	public $conta;
    public $xml;

	public function __construct(){
		parent::__construct();
        $this->load->model('clientes_model', 'clientes');
        $this->load->model('faturas_model', 'faturas');
        $this->load->model('temas_model', 'temas');
        $this->load->model('contas_model', 'contas');
        //$this->load->model('financeiro_model', 'financeiro');
        $this->load->model('gateways_model', 'gateways');

        //$this->cor_default = $this->temas->getCor();
		//$this->logotipo = $this->temas->getLogo();

        /*
        if(!$this->users->isOk()){
        	die(redirect('erro404'));
        }
        */

        $hash = getVars();
        $this->xml = json_decode(getJSON(site_url('api/getFatura/json?'.$hash)));
	}

	public function index(){

        if(isset($this->xml->erro)){
            echo utf8_decode($this->xml->erro);
            die();
        }

        $data['xml'] = $this->xml->resultado;
		$this->load->view('fatura/template_fatura', $data);
	}

    public function initJs($gateway){
        $dados = $this->input->post();
        $config = $this->gateways->getConfig($dados['contaId'], $gateway);

        if($gateway == "boleto"){
            $boletoArray = explode("_", $config->tipo_boleto);

            if(isset($boletoArray[1])){
                $gatewayBoleto = $boletoArray[1];
                $configBoleto = $this->gateways->getConfig($dados['contaId'], $gatewayBoleto);
                
                $post = array(
                    'config' => $configBoleto,
                    'post' => $dados,
                    'gateway' => $gatewayBoleto
                );

                echo json_encode($post);

            }else{
                echo "Boleto Próprio";
            }
        }else{
            $post = array(
                'config' => $config,
                'post' => $dados,
                'gateway' => $gateway
            );

            echo json_encode($post);
        }
    }

    public function pagamento($gateway){
        $dados = $this->input->post();
        $config = $this->gateways->getConfig($dados['contaId'], $gateway);
        
        if($gateway == "boleto"){
            $boletoArray = explode("_", $config->tipo_boleto);

            if(isset($boletoArray[1])){
                $gatewayBoleto = $boletoArray[1];
                $configBoleto = $this->gateways->getConfig($dados['contaId'], $gatewayBoleto);
                
                $post = array(
                    'config' => $configBoleto,
                    'post' => $dados,
                    'gateway' => $gatewayBoleto
                );

                $url = site_url('gateways/'.$gatewayBoleto.'/boletoDireto.php');
                jsonPOST($url, array('dados' => json_encode($post)));

            }else{
                echo "Boleto Próprio";
            }
        }
    }

	
}

?>