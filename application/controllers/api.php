<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller{
    
    private $usuario;
    
    public function __construct(){
        parent::__construct();

        $this->load->model('clientes_model' , 'clientes');
        $this->load->model('faturas_model'  , 'faturas');
        $this->load->model('contas_model'   , 'contas');
        $this->load->model('gateways_model' , 'gateways');
    }

    /* Contas ***********************************************************************/
    public function novaConta($tipo = 'xml'){
        $post = array(
            'auth' => md5('meutiooperou.com.br_api2cart'),
            'email' => 'lucas.codemax@gmail.com',
            'dominio' => 'meutiooperou.com.br'
        );

        if(!$this->contas->verificaExiste($post['auth'])){
            if($this->contas->novo($post)){
                $retorno = array(
                    'status' => 'sucesso',
                    'mensagem' => 'Conta cadastrada com sucesso.'
                );
            }else{
                $retorno = array(
                    'status' => 'erro',
                    'mensagem' => 'Não foi possível cadastrar a nova Conta.'
                );
            }
        }else{
            $retorno = array(
                'status' => 'erro',
                'mensagem' => 'Esta conta já existe'
            );
        }

        echo ($tipo == "json") ? json_encode(array('resultado' => $retorno)) : arrayToXML($retorno);
    }

    public function excluirConta($tipo = 'xml'){
        $post = array(
            'auth' => md5('meutiooperou.com.br_api2cart')
        );

        if($this->contas->verificaExiste($post['auth'])){
            if($this->contas->excluir($post['auth'])){
                $retorno = array(
                    'status' => 'sucesso',
                    'mensagem' => 'Conta excluída com sucesso.'
                );
            }else{
                $retorno = array(
                    'status' => 'erro',
                    'mensagem' => 'Erro ao tentar excluir a conta.'
                );
            }
        }else{
            $retorno = array(
                'status' => 'erro',
                'mensagem' => 'Esta conta não existe'
            );
        }

        if($tipo == "xml"){
            echo arrayToXML($retorno);
        }else{
            echo json_encode(array('resultado' => $retorno));
        }
    }
    
    private function isLogged(){
        return TRUE;
    }

    /* Faturas **********************************************************************/
    public function novaFatura($tipo = 'xml'){
        $post = array(
            'faturaCliente' => 1,
            'faturaValor' => 30.90,
            'faturaVencimento' => somaData_timestamp(date('Y-m-d H:i:s'), 5, 0, 0),
            'faturaTipo' => 'pedido',
            'contaID' => 1,
            'faturaItens' => array(
                array(
                    'produtoId' => 12,
                    'produtoDescricao' => 'Tennis Oakley',
                    'produtoPreco' => 10.90,
                    'produtoDominio' => 'codemax.com.br',
                    'produtoQtd' => 1,
                    'produtoCiclo' => 'mensal'
                ),
                array(
                    'produtoId' => 13,
                    'produtoDescricao' => 'Monitor OAC 19 Polegadas',
                    'produtoPreco' => 20.00,
                    'produtoDominio' => 'codemax.com.br',
                    'produtoQtd' => 1,
                    'produtoCiclo' => 'anual'
                )
            )
        );
        
        if($this->faturas->setFatura($post)){
            $retorno = array(
                'status' => 'sucesso',
                'mensagem' => 'Fatura gerada com sucesso.'
            );
        }else{
            $retorno = array(
                'status' => 'erro',
                'mensagem' => 'Não foi possível gerar a nova Fatura.'
            );
        }

        echo ($tipo == "json") ? json_encode(array('resultado' => $retorno)) : arrayToXML($retorno);
    }

    public function getFatura($tipo = 'xml'){

        if($this->isLogged()){
            // Verificamos se foi passado um parâmetro de busca "id"
            $hash = getVars();
            $id_fatura = $this->input->get('id');

            if($id_fatura || $hash != NULL){

                if($fatura = $this->faturas->getById($id_fatura, $hash)){
                   $fatura_itens = $this->faturas->getItens($fatura->fatura_id);

                   $retorno = array(
                        'fatura_id' => $fatura->fatura_id,
                        'fatura_tipo' => $fatura->fatura_tipo,
                        'fatura_emissao' => $fatura->fatura_emissao,
                        'fatura_vencimento' => $fatura->fatura_vencimento,
                        'fatura_update' => $fatura->fatura_update,
                        'fatura_valor' => $fatura->fatura_valor,
                        'fatura_hash' => $fatura->fatura_hash,
                        'fatura_status' => $fatura->fatura_status,
                        'cliente' => array(
                            'cliente_id' => $fatura->cliente_id,
                            'tipo_pessoa' => $fatura->cliente_pessoa,
                            'razao_nome' => $fatura->cliente_nome,
                            'cnpj' => $fatura->cliente_cnpj,
                            'cpf' => $fatura->cliente_cpf,
                            'nascimento' => $fatura->cliente_nascimento,
                            'inscricao_municipal' => $fatura->cliente_inscricao_municipal,
                            'inscricao_estadual' => $fatura->cliente_inscricao_estadual,
                            'email' => $fatura->cliente_email,
                            'email_cobranca' => $fatura->cliente_email_cobranca,
                            'telefone' => $fatura->cliente_telefone,
                            'celular' => $fatura->cliente_celular,
                            'cep' => $fatura->cliente_cep,
                            'logradouro' => $fatura->cliente_logradouro,
                            'numero' => $fatura->cliente_numero,
                            'complemento' => $fatura->cliente_complemento,
                            'uf' => $fatura->cliente_uf,
                            'cidade' => $fatura->cliente_cidade,
                            'bairro' => $fatura->cliente_bairro,
                            'responsavel' => $fatura->cliente_responsavel,
                            'responsavel_cpf' => $fatura->cliente_responsavel_cpf
                        ),
                        'fatura_itens' => array(),
                        'conta_id' => $fatura->conta
                    );

                    foreach ($fatura_itens as $item):
                        $item_add = array(
                            'produto_id' => $item->id,
                            'dominio' => $item->dominio,
                            'descricao' => $item->descricao,
                            'preco' => $item->preco,
                            'ciclo' => $item->ciclo,
                            'inicio' => $item->inicio,
                            'vencimento' => $item->final
                        );

                        array_push($retorno['fatura_itens'], $item_add);

                    endforeach;

                }else{
                    $retorno = array(
                        'status' => 'erro',
                        'mensagem' => 'Fatura inválida!'
                    );
                }
            }else{
                $retorno = array(
                    'status' => 'erro',
                    'mensagem' => 'O GET/HTTP "id" é obrigatório'
                );
            }
        }else{
            $retorno = array(
                'status' => 'erro',
                'mensagem' => 'Acesso Negado.'
            );
        }

        echo ($tipo == "json") ? json_encode(array('resultado' => $retorno)) : arrayToXML($retorno);
    }

    public function excluirFatura($tipo = 'xml'){

    }
    
    /* Clientes *********************************************************************/
    public function novoCliente($tipo = 'xml'){

    }

    public function getCliente($tipo = 'xml'){
        
        if($this->isLogged()){
            if($id_cliente = $this->input->get('id')){
                $count_cliente = $this->clientes->count_id($id_cliente);

                if($count_cliente > 0){
                    $cliente = $this->clientes->getById($id_cliente);
                    
                    unset($cliente->conta_id);
                    $retorno = $cliente;                    

                }else{
                    $retorno = array(
                        'status' => 'erro',
                        'mensagem' => 'Cliente não encontrado.'
                    );
                }
            }else{
                $retorno = array(
                    'status' => 'erro',
                    'mensagem' => 'O GET/HTTP "id" é obrigatório.'
                );
            }
        }else{
            $retorno = array(
                'status' => 'erro',
                'mensagem' => 'Acesso Negado.'
            );
        }

        echo ($tipo == "json") ? json_encode(array('resultado' => $retorno)) : arrayToXML($retorno);
    }
    
    public function excluirCliente($tipo = 'xml'){
        
        if($this->isLogged()){
            if($id_cliente = $this->input->get('id')){
                $count_cliente = $this->clientes->count_id($id_cliente);
                
                if($count_cliente > 0){
                    if($sql = $this->clientes->excluir($id_cliente)){
                        $retorno = array(
                            'status' => 'sucesso',
                            'mensagem' => 'Cliente removido com sucesso.'
                        );
                    }else{
                        $retorno = array(
                            'status' => 'erro',
                            'mensagem' => 'Ocorreu um erro ao tentar excluir o cliente.'
                        );
                    }
                }else{
                    $retorno = array(
                        'status' => 'erro',
                        'mensagem' => 'Cliente não encontrado.'
                    );
                }
            }else{
                $retorno = array(
                    'status' => 'erro',
                    'mensagem' => 'O GET/HTTP "id" é obrigatório.'
                );
            }
        }else{
            $retorno = array(
                'status' => 'erro',
                'mensagem' => 'Acesso Negado.'
            );
        }

        echo ($tipo == "json") ? json_encode(array('resultado' => $retorno)) : arrayToXML($retorno);
    }

    /* Gateways *********************************************************************/

    public function atualizaConfig($tipo = 'xml'){

    }

    public function getGatewaysAtivos($tipo = 'xml'){
        $conta = md5('codemax.com.br_api2cart');
        $accGateway = $this->gateways->gatewaysAtivos($conta);

        if($accGateway != NULL){
            $retorno = $accGateway;
        }else{
            $retorno = array(
                'status' => 'erro',
                'mensagem' => 'Cliente não encontrado.'
            );
        }

        echo ($tipo == "json") ? json_encode(array('resultado' => $retorno)) : arrayToXML($retorno);
    }

    public function novoConfig($tipo = 'xml'){
        $gateway = 'pagseguro';
        $conta = 1;
        $config = array(
            'email' => 'lucas.codemax@gmail.com',
            'token' => '0E5BB306F8D34E40ABF11DE44EA6921E',
            'sandbox' => true
        );

        if($model = $this->gateways->novoConfig($gateway, $conta, $config)){
            $retorno = array(
                'status' => 'sucesso',
                'mensagem' => 'Gateway configurado com sucesso.'
            );
        }else{
            $retorno = array(
                'status' => 'erro',
                'mensagem' => 'Ocorreu um erro ao tentar configurar o gateway. Por favor, tente novamente.'
            );
        }

        echo ($tipo == "json") ? json_encode(array('resultado' => $retorno)) : arrayToXML($retorno);
    }

    public function pagarBoleto($tipo = 'xml'){
        $faturaId = 1; //<-- POST fatura_id
        $gateway = 'GerenciaNet'; // <-- POST gateway

        if ($fatura = $this->faturas->getById($faturaId)){

            print_r($fatura);
            
            $post = array('post' => $fatura);

            //$url = site_url('gateways/'.$gateway.'/boletoDireto.php');
            //$curl = postCURL($url, $post);

        }else{
            echo "Fatura inválida.";
        }
    }
    
    /* Transações ********************************************************************/

    public function novaTransacao($tipo = 'xml'){
        $add = $this->input->post();

        if($this->gateways->addTransacao($add)){
            $retorno = array(
                'status' => 'sucesso',
                'mensagem' => 'Transacao adicionada com sucesso.'
            );
        }else{
            $retorno = array(
                'status' => 'erro',
                'mensagem' => 'Ocorreu um erro ao tentar adicioanr a transacao.'
            );
        }

        echo ($tipo == "json") ? json_encode(array('resultado' => $retorno)) : arrayToXML($retorno);
    }

    public function buscaTransacao($tipo = 'xml'){
        $busca = $this->input->post();
        $api = $this->gateways->verificaTransacao($busca);

        if(!$api){
            echo "gera";
        }else{
            echo json_encode($api);
        }
    }
}

?>